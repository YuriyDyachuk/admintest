****<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Start project Laravel
##  To open out a project with commands:

<div class="container">
    <span><i class="fa fa-unlock" aria-hidden="true">composer install:</i></span>
    <span><i class="fa fa-database" aria-hidden="true">php artisan migrate:</i></span>
    <span><i class="fa fa-css3" aria-hidden="true">npm run dev:</i></span>
</div>

- [Not composer](https://getcomposer.org/download/).
- [npm and node download](https://nodejs.org/uk/download/).


